#!/usr/bin/env julia

function line_to_array(line::AbstractString)
    parse.(UInt8, replace(line, '.' => 0, '/' => 1, '\\' => 2, '-' => 3, '|' => 4) |> collect)
end

function get_matrix(lines)::Matrix{UInt8}
    matrix = []
    for line in lines
        push!(matrix, line_to_array(line))
    end
    # matrix is rotated => balls roll to the east, not north
    hcat(matrix...)'
end

const RIGHT = 1
const UP = 2
const LEFT = 4
const DOWN = 8
# bit positions
const RIGHT_P = 0
const UP_P = 1
const LEFT_P = 2
const DOWN_P = 3

function dirs_to_string(dirs)
    s = ""
    if dirs & UP ≠ 0
        s *= "UP,"
    end
    if dirs & RIGHT ≠ 0
        s *= "RIGHT,"
    end
    if dirs & LEFT ≠ 0
        s *= "LEFT,"
    end
    if dirs & DOWN ≠ 0
        s *= "DOWN,"
    end
    if s == ""
        println("You fucked up, A-aron!")
        @show dirs
        exit()
    end
    s[1:end-1]
end

function swap_bits(n, p1, p2)
    # Move p1'th to rightmost side
    bit1 = (n >> p1) & 1
    # Move p2'th to rightmost side
    bit2 = (n >> p2) & 1
    # XOR the two bits
    x = (bit1 ⊻ bit2)
    # Put the xor bit back to their original positions
    x = (x << p1) | (x << p2)
    # XOR 'x' with the original number so that the
    # two sets are swapped
    n = n ⊻ x
end

function move(tile, dir)
    if tile == 0
        dir
    elseif tile == 1
        # swap UP with RIGHT and DOWN with LEFT
        dir = swap_bits(dir, RIGHT_P, UP_P)
        swap_bits(dir, DOWN_P, LEFT_P)
    elseif tile == 2
        # swap UP with LEFT and DOWN with RIGHT
        dir = swap_bits(dir, UP_P, LEFT_P)
        swap_bits(dir, DOWN_P, RIGHT_P)
    elseif tile == 3
        if ((dir & UP) | (dir & DOWN)) ≠ 0
            RIGHT | LEFT
        else
            dir
        end
    else
        if ((dir & LEFT) | (dir & RIGHT)) ≠ 0
            UP | DOWN
        else
            dir
        end
    end
end

function walk!(matrix, row, col, dir)
    rows, cols = size(matrix)
    if row < 1 || row > rows || col < 1 || col > cols
        # out of bounds
        return
    end
    if ((matrix[row, col] >> 4) & dir) ≠ 0
        # we have already walked in this direction on this tile
        return
    end
    #dir_str = dirs_to_string(dir)
    #println("Walking on $row, $col into direction $dir_str")
    # mark in the matrix that we are walking this direction
    matrix[row, col] |= dir << 4
    dir = move(matrix[row, col] & 0b111, dir)
    if dir & RIGHT ≠ 0
        #println("RIGHT")
        walk!(matrix, row, col + 1, RIGHT)
    end
    if dir & LEFT ≠ 0
        #println("LEFT")
        walk!(matrix, row, col - 1, LEFT)
    end
    if dir & UP ≠ 0
        #println("UP")
        walk!(matrix, row - 1, col, UP)
    end
    if dir & DOWN ≠ 0
        #println("DOWN")
        walk!(matrix, row + 1, col, DOWN)
    end
end

function count_energised_tiles(matrix)
    sum = 0
    rows, cols = size(matrix)
    for row in 1:rows
        for col in 1:cols
            if matrix[row, col] & 0b11110000 ≠ 0
                sum += 1
            end
        end
    end
    sum
end

function main()
    lines = readlines("input")
    matrix = get_matrix(lines)
    rows, cols = size(matrix)
    counts = []
    for dir in [RIGHT, DOWN, UP, LEFT]
        (row_range, col_range) = if dir == RIGHT
            1:rows, 1:1
        elseif dir == UP
            rows:rows, 1:cols
        elseif dir == LEFT
            1:rows, cols:cols
        else
            1:1, 1:cols
        end
        for row in row_range
            for col in col_range
                cur_matrix = copy(matrix)
                walk!(cur_matrix, row, col, dir)
                dir_str = dirs_to_string(dir)
                println("Config: $row, $col, $dir_str")
                push!(counts, count_energised_tiles(cur_matrix))
                @show counts[end]
            end
        end
    end
    println(maximum(counts))
end

main()
