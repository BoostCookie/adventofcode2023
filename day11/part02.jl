#!/usr/bin/env julia

function get_galaxies(lines)::Array{Tuple{Int, Int}}
    galaxies = []
    for (row, line) ∈ enumerate(lines)
        for col ∈ findall('#', line)
            push!(galaxies, (row, col))
        end
    end
    galaxies
end

function adjust_for_inflation!(galaxies::Array{Tuple{Int, Int}})
    minrow = minimum(galaxies)[1]
    maxrow = maximum(galaxies)[1]
    # for the rows
    for row ∈ maxrow:-1:minrow
        if !any(g -> g[1] == row, galaxies)
            # empty row found => increase row coordinate of all galaxies below
            for (i, galaxy) in filter(i_g -> i_g[2][1] > row, galaxies |> enumerate |> collect)
                galaxies[i] = (galaxy[1] + 999999, galaxy[2])
            end
        end
    end
    # for the columns
    mincol = minimum(map(g -> g[2], galaxies))
    maxcol = maximum(map(g -> g[2], galaxies))
    for col ∈ maxcol:-1:mincol
        if !any(g -> g[2] == col, galaxies)
            # empty column found => increase column coordinate of all galaxies to the right
            for (i, galaxy) in filter(i_g -> i_g[2][2] > col, galaxies |> enumerate |> collect)
                galaxies[i] = (galaxy[1], galaxy[2] + 999999)
            end
        end
    end
end

function distance(galaxy1, galaxy2)
    abs(galaxy1[1] - galaxy2[1]) + abs(galaxy1[2] - galaxy2[2])
end

function main()
    lines = readlines("input")
    galaxies = get_galaxies(lines)
    adjust_for_inflation!(galaxies)
    sum = 0
    for (i, galaxy1) in enumerate(galaxies)
        for galaxy2 in galaxies[i:end]
            sum += distance(galaxy1, galaxy2)
        end
    end
    println(sum)
end

main()
