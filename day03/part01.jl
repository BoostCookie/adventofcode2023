#!/usr/bin/env julia

function adjacent_symbol(engine, row, col)
    for i ∈ max(1, row-1):min(length(engine), row+1)
        for j ∈ max(1, col-1):min(length(engine[i]), col+1)
            if i == row && j == col; continue; end
            if engine[i][j] ≠ '.' && tryparse(Int, engine[i][j:j]) === nothing
                return true
            end
        end
    end
    false
end

function main()
    sum = 0
    engine = readlines("input")
    for row ∈ 1:length(engine)
        col = 1
        while col ≤ length(engine[row])
            # find next number
            if tryparse(Int, engine[row][col:col]) !== nothing
                cur_num = 0
                col_end = col
                while col_end ≤ length(engine[row])
                    new_num = tryparse(Int, engine[row][col:col_end])
                    if new_num === nothing
                        break
                    end
                    cur_num = new_num
                    col_end += 1
                end
                # check if the number has adjacent symbols
                print("Checking $cur_num at $row, $col\t")
                if any(c -> adjacent_symbol(engine, row, c), col:col_end-1)
                    println("YES")
                    sum += cur_num
                else
                    println("NO")
                end
                col = col_end - 1
            end
            col += 1
        end
    end
    println(sum)
end

if abspath(PROGRAM_FILE) == @__FILE__
    main()
end
