#!/usr/bin/env julia

function get_num_at(engine, row, col)
    # get starting col
    starting_col = col
    for j ∈ col:-1:1
        if tryparse(Int, engine[row][j:col]) === nothing
            break
        end
        starting_col = j
    end
    # get num and ending col
    num = 0
    ending_col = col
    for j ∈ col:length(engine[row])
        new_num = tryparse(Int, engine[row][starting_col:j])
        if new_num === nothing
            break
        end
        num = new_num
        ending_col = j
    end
    num, starting_col, ending_col
end

function adjacent_numbers(engine, row, col)
    numbers = []
    i = max(1, row-1)
    while i ≤ min(length(engine), row+1)
        j = max(1, col-1)
        while j ≤ min(length(engine[i]), col+1)
            if tryparse(Int, engine[i][j:j]) !== nothing
                num, _, ending_col = get_num_at(engine, i, j)
                push!(numbers, num)
                j = ending_col
            end
            j += 1
        end
        i += 1
    end
    numbers
end

function main()
    sum = 0
    engine = readlines("input")
    for row ∈ 1:length(engine)
        col = 1
        while col ≤ length(engine[row])
            col = findnext("*", engine[row], col)
            if col === nothing
                break
            else
                col = col[1]
            end
            adjnums = adjacent_numbers(engine, row, col)
            if length(adjnums) == 2
                sum += *(adjnums...)
            end
            col += 1
        end
    end
    println(sum)
end

if abspath(PROGRAM_FILE) == @__FILE__
    main()
end
