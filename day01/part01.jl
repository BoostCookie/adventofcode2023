#!/usr/bin/env julia

matchvalues = Dict([("1", 1), ("2", 2), ("3", 3), ("4", 4), ("5", 5), ("6", 6), ("7", 7), ("8", 8), ("9", 9)])

function get_match(input::AbstractString, first=true)
    matchfunction = first ? startswith : endswith
    inputrange = if first; 1:length(input) else length(input):-1:1 end
    for i ∈ inputrange
        substring = if first; input[i:end] else input[1:i] end
        for key ∈ keys(matchvalues)
            if matchfunction(substring, key)
                return get(matchvalues, key, 0)
            end
        end
    end
    0
end

function main()
    sum = 0
    for line ∈ readlines("input")
        startnum = get_match(line, true)
        endnum = get_match(line, false)
        sum += 10 * startnum + endnum
    end
    println(sum)
end

if abspath(PROGRAM_FILE) == @__FILE__
    main()
end
