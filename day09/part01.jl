#!/usr/bin/env julia

function pairwise(iterable)
    return zip(iterable[1:end-1], iterable[2:end])
end

function create_diffslist(history)
    diffslist = [history]
    while true
        newlist = []
        allzeros = true
        for (a, b) in pairwise(diffslist[end])
            diff = b - a
            allzeros &= diff == 0
            push!(newlist, diff)
        end
        push!(diffslist, newlist)
        if allzeros
            break
        end
    end
    diffslist
end

function extrapolate(history)
    diffslist = create_diffslist(history)
    prev_element = 0
    for diffs in diffslist[end-1:-1:1]
        prev_element = diffs[end] + prev_element
    end
    prev_element
end

function get_nums(line)
    parse.(Int, split(line, " "))
end

function main()
    lines = readlines("input")
    histories = lines .|> get_nums
    histories .|> extrapolate |> sum |> println
end

main()
