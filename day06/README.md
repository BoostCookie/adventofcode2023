Upon compilation of the typst document using
```
typst compile day06.typ
```
the `input` file is read and interpreted.

The compiled pdf can be viewn here [https://gitlab.com/BoostCookie/adventofcode2023/-/jobs/artifacts/main/raw/day06/day06.pdf?job=compile_pdf](https://gitlab.com/BoostCookie/adventofcode2023/-/jobs/artifacts/main/raw/day06/day06.pdf?job=compile_pdf).
