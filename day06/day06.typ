#set math.equation(numbering: "(1)")
#let mathref(label) = {
  ref(label, supplement: "")
}
#let equalref(labelstr) = {
  $attach(=, t: (#ref(label(labelstr), supplement: h(-0.4em))))$
}
#let is_not_empty(str) = {
  str.len() != 0
}
#let line_to_nums(line) = {
  line.split(":").at(1).split(" ").filter(is_not_empty).map(int)
}
#let num(T, x) = {
  let T2 = T / 2
  let D = calc.sqrt(calc.pow(T2, 2) - x)
  calc.round(T2 + D + 0.5) - calc.round(T2 - D + 0.5)
}

#align(center, text(17pt)[
  Advent of Code 2023 \
  2023-12-06 \
])

#align(center)[Gehr, Stefan]

= Theory
In the race we have a waiting time $t_w$ and a travel time $t_t$ and they need to add up to the total race time
$ t_w + t_t = T. $ <totaltime>
After the waiting time $t_w$ we have a constant velocity
$ v(t_w) = a t_w $ <velocity>
and in our case $a = 1 space.hair "mm"/"ms"^2$.
During the race we travel the distance $x$ with
$ x = v(t_w) t_t equalref("velocity") a t_w t_t equalref("totaltime") a t_w (T - t_w) = - a t_w^2 + a T t_w \
  <==> 0 = t_w^2 - T t_w + x/a. $ <parabola>
This is a quadratic equation with respect to our desired variable $t_w$.
Solving it gives us
$ t_(w, plus.minus)(T, x) = T/2 plus.minus sqrt((T/2)^2 - x/a). $ <quadratic>
For $x$ we insert the previous distance record and we get the two times $t_(w,+)(T,x)$ and $t_(w,-)(T,x)$ which achieve exactly these times.
Since @parabola is a parabola which opens upwards, putting in a time $t_W$, with $t_(w,-)(T,x) < t_W < t_(w,+)(T,x)$, gives us
$ 0 > t_W^2 - T t_W + x/a \
<==> x < a T t_W - a t_W^2 = a t_W (T - t_W)
= v(t_W) t_T, $
where $t_T = T - t_W$ is the travel time corresponding to the waiting time $t_W$.
So these in-between times $t_W$ beat the old record.
We want to find out how many such times exist when we are only allowed to use multiples of $1 "ms"$.
To get the number of integers between $a in RR^+$ and $b in RR^+$ (excluding the borders themselves if they are also integers), with $a>=b$, we use
$ n(a, b) = "round"(a + 0.5) - "round"(b + 0.5). $ <integerfun>
For the integer $k in NN$ this half-rounding function $a |-> "round"(a + 0.5)$ maps
$ [k, k+1) |-> {k+1}. $
We substitute $(t_(w, plus.minus))/(1 "ms")$ for $a$ and $b$ and get
$ N(T,x) &= n((t_(w, +)(T,x))/(1 "ms"), (t_(w,-)(T,x))/(1 "ms")) \
&equalref("integerfun") "round"((t_(w,+)(T,x))/(1 "ms") + 0.5) - "round"((t_(w,-)(T,x))/(1 "ms") + 0.5) \
&equalref("quadratic")
"round"((T/2 + sqrt((T/2)^2 - x / a)) / (1 "ms") + 0.5)
- "round"((T/2 - sqrt((T/2)^2 - x / a)) / (1 "ms") + 0.5). $ <nequation>

= Inserting the Numbers
== Part 1
We create a nice table where for each $T$ and $x$ we calculate $N(T,x)$.
#let lines = read("input").split("\n")
#let times = line_to_nums(lines.at(0))
#let distances = line_to_nums(lines.at(1))
#let times_distances = times.zip(distances)
#let nums = times_distances.map(v => num(v.at(0), v.at(1)))
#let tablecontent = times.zip(distances, nums).map(v => ([#v.at(0)], [#v.at(1)], [#v.at(2)])).flatten()
#table(
  columns: (auto, auto, auto),
  inset: 10pt,
  align: center,
  $T / (1 "ms")$, $x / (1 "mm")$, $N(T, x)$,
  ..tablecontent
)
All we have to do in the end is to multiply all the values of $N(T,x)$ which gives us
$ product_((T, x))   N(T,x) = #nums.product(). $

== Part 2
#let T = int(times.map(v => str(v)).join())
#let x = int(distances.map(v => str(v)).join())
We concatenate all the digits of the $T$ values in our table to get a new single value which is
$ T = #T "ms". $
We do the same for $x$ and get
$ x = #x "mm". $
We insert these numbers into @nequation and get
$ N(T,x) = #num(T, x). $
