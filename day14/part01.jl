#!/usr/bin/env julia
using LinearAlgebra

function line_to_array(line)
    parse.(Int8, replace(line, '.' => 0, 'O' => 1, '#' => 2) |> collect)
end

function pairwise(iterable)
    zip(iterable[1:end-1], iterable[2:end])
end

function get_matrix(lines)::Matrix{Int8}
    matrix = []
    for line in lines
        println(line)
        insert!(matrix, 1, line_to_array(line))
    end
    # matrix is rotated => balls roll to the east, not north
    hcat(matrix...)
end

function calc_row_load(row)
    res = 0
    cuberock_indices = findall(x -> x==2, row)
    # imaginative borders at beginning and end
    insert!(cuberock_indices, 1, 0)
    push!(cuberock_indices, length(row)+1)
    for (i, j) in pairwise(cuberock_indices)
        n = sum(row[i+1:j-1])
        res += n * j - n * (n +1) ÷ 2
    end
    res
end

function main()
    lines = readlines("input")
    matrix = get_matrix(lines)
    display(matrix)
    res = 0
    for row in 1:size(matrix)[1]
        res += calc_row_load(matrix[row,:])
    end
    println(res)
end

main()
