#!/usr/bin/env julia
using LinearAlgebra

function line_to_array(line)
    parse.(Int8, replace(line, '.' => 0, 'O' => 1, '#' => 2) |> collect)
end

function pairwise(iterable)
    zip(iterable[1:end-1], iterable[2:end])
end

function get_matrix(lines)::Matrix{Int8}
    matrix = []
    for line in lines
        println(line)
        insert!(matrix, 1, line_to_array(line))
    end
    # matrix is rotated => balls roll to the east, not north
    hcat(matrix...)
end

function calc_row_load(row)
    row[findall(x -> x==2, row)] .= 0
    sum(row .* collect(1:length(row)))
end

function roll_right(row)
    cuberock_indices = findall(x -> x==2, row)
    # imaginative borders at beginning and end
    insert!(cuberock_indices, 1, 0)
    push!(cuberock_indices, length(row)+1)
    for (i, j) in pairwise(cuberock_indices)
        n = sum(row[i+1:j-1])
        row[j-n:j-1] = ones(Int8, n)
        row[i+1:j-n-1] = zeros(Int8, j-n-i-1)
    end
    row
end

function cycle!(matrix)
    rows, cols = size(matrix)
    # north
    for row in 1:rows
        matrix[row,:] = roll_right(matrix[row,:])
    end
    #println("north")
    #display(matrix)
    # west
    for col in 1:cols
        matrix[rows:-1:1,col] = roll_right(matrix[rows:-1:1,col])
    end
    #println("west")
    #display(matrix)
    # south
    for row in 1:rows
        matrix[row,cols:-1:1] = roll_right(matrix[row,cols:-1:1])
    end
    #println("south")
    #display(matrix)
    # east
    for col in 1:cols
        matrix[:,col] = roll_right(matrix[:,col])
    end
    #println("east")
    #display(matrix)
end

function main()
    lines = readlines("input")
    matrix = get_matrix(lines)
    display(matrix)
    matrix_history::Array{Matrix{Int8}} = []
    cycle_count = 0
    while matrix ∉ matrix_history && cycle_count < 1_000_000_000
        push!(matrix_history, copy(matrix))
        cycle!(matrix)
        cycle_count += 1
    end

    if cycle_count < 1_000_000_000
        # there is a repeating pattern
        cycles_left = 1_000_000_000 - cycle_count
        pattern_start = findfirst(m -> m == matrix, matrix_history)
        pattern_length = length(matrix_history) - pattern_start + 1
        matrix = matrix_history[pattern_start + cycles_left % pattern_length]
    end

    res = 0
    for row in 1:size(matrix)[1]
        res += calc_row_load(matrix[row,:])
    end
    println(res)
end

main()
