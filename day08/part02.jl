#!/usr/bin/env julia

function analyse_cycle(walk, worldmap, startpos)
    @show startpos
    startpositions = [startpos]
    zpositions::Array{Int} = []
    count = 0
    currentpos = startpos
    while true
        for step ∈ walk
            count += 1
            currentpos = worldmap[currentpos][step]
            if currentpos[end] == 'Z'
                push!(zpositions, count)
            end
        end
        startpos = currentpos
        if startpos ∈ startpositions
            break
        else
            push!(startpositions, startpos)
        end
    end
    @show startpositions
    @show startpos
    @show zpositions
    # from the printouts it is obvious that all the cycles have these things in common:
    # 1. The closed cycle starts after one full walk
    # 2. A z position only comes up once in every cycle and it is the end position of the
    #    second-to-last walk (and therefore the start position of the last walk after
    #    which the cycle continues
    # With this in mind we first do
    # length(startpositions) - 1
    # walks, and therefore
    # (length(startpositions) - 1) * length(walk)
    # steps. Now we are at a zposition and it also comes up every
    # (length(startpositions) - 1) * length(walk)
    # steps. HOW CONVENIENT?
    # So we just need to find the lowest common multiple between each
    # length(startpositions) - 1
    # and multiply this with length(walk) to get the number of steps after which all the
    # courses are at a zposition.
    length(startpositions) - 1
end

function get_starting_positions(worldmap)
    starts::Array{AbstractString} = []
    for key ∈ keys(worldmap)
        if key[end] != 'A'
            continue
        end
        push!(starts, key)
    end
    starts
end

function walk_from_a_to_z(walk, worldmap)
    count = 0
    start_positions = get_starting_positions(worldmap)
    cycle_lengths = map(p -> analyse_cycle(walk, worldmap, p), start_positions)
    lcm(cycle_lengths...) * length(walk)
end

function lr_to_num(lr)
    if lr == 'L'
        1
    else
        2
    end
end

function interpret_input(lines)
    walk = lines[1] |> collect .|> lr_to_num
    worldmap::Dict{AbstractString, Tuple{AbstractString, AbstractString}} = Dict()
    for line ∈ lines[3:end]
        start, rest = split(line, " = (")
        start = string(start)
        left = string(rest[1:3])
        right = string(rest[6:8])
        worldmap[start] = (left, right)
    end
    walk, worldmap
end

function main()
    lines = readlines("input")
    walk, worldmap = interpret_input(lines)
    walk_from_a_to_z(walk, worldmap) |> println
end

main()
