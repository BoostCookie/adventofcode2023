#!/usr/bin/env julia

function walk_from_aaa_to_zzz(walk, map)
    count = 0
    current_pos = "AAA"
    for step ∈ Iterators.cycle(walk)
        count += 1
        current_pos = map[current_pos][step]
        if current_pos == "ZZZ"
            break
        end
    end
    count
end

function lr_to_num(lr)
    if lr == 'L'
        1
    else
        2
    end
end

function interpret_input(lines)
    walk = lines[1] |> collect .|> lr_to_num
    map::Dict{AbstractString, Tuple{AbstractString, AbstractString}} = Dict()
    for line ∈ lines[3:end]
        start, rest = split(line, " = (")
        left = rest[1:3]
        right = rest[6:8]
        map[start] = (left, right)
    end
    walk, map
end

function main()
    lines = readlines("input")
    walk, map = interpret_input(lines)
    walk_from_aaa_to_zzz(walk, map) |> println
end

main()
