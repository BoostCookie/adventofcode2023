#!/usr/bin/env julia

using LinearAlgebra

function line_to_array(line)
    parse.(Int8, replace(line, '.' => 0, '#' => 1) |> collect)
end

function get_patterns(lines)
    patterns::Array{Matrix{Int8}} = []
    cur_pattern = []
    for line in lines
        if line == ""
            push!(patterns, hcat(cur_pattern...)')
            cur_pattern = []
            display(patterns[end])
            println()
            continue
        end
        println(line)
        push!(cur_pattern, line_to_array(line))
    end
    if cur_pattern ≠ []
        push!(patterns, hcat(cur_pattern...)')
        display(patterns[end])
    end
    patterns
end

function get_symmetry(pattern)
    display(pattern)
    rows, cols = size(pattern)
    # check the rows
    for row in 1:rows-1
        issym = true
        for offset in 0:(min(row, rows-row)-1)
            #=
            @show row
            @show offset
            @show pattern[row-offset, :]
            @show pattern[row+offset+1, :]
            =#
            if pattern[row-offset,:] ≠ pattern[row+offset+1,:]
                issym = false
                break
            end
        end
        if issym
            println("row $row")
            return row, true
        end
    end
    # check the cols
    for col in 1:cols-1
        issym = true
        for offset in 0:(min(col, cols-col)-1)
            #=
            @show col
            @show offset
            @show pattern[:, col-offset]
            @show pattern[:, col+offset+1]
            =#
            if pattern[:, col-offset] ≠ pattern[:, col+offset+1]
                issym = false
                break
            end
        end
        if issym
            println("col $col")
            return col, false
        end
    end
    println("You done messed up, Aaron!")
    exit()
end

function main()
    lines = readlines("input")
    patterns = get_patterns(lines)
    sum = 0
    for pattern in patterns
        index, isrow = get_symmetry(pattern)
        sum += isrow ? 100 * index : index
        @show sum
    end
    println(sum)
end

main()
