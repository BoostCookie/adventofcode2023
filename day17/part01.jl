#!/usr/bin/env julia

using AStarSearch

function parse_line(line)
    parse.(Int, collect(line))
end

const UPSHIFT = 0
const RIGHTSHIFT = 2
const DOWNSHIFT = 4
const LEFTSHIFT = 6

function neighbours(lines, state::Tuple{Int, Int, UInt8})::Array{Tuple{Int, Int, UInt8}}
    rows = length(lines)
    cols = length(lines[1])
    row, col, accumulated_dir = state
    ret = []
    accumulated_up = (accumulated_dir >> UPSHIFT) & 0b11
    accumulated_right = (accumulated_dir >> RIGHTSHIFT) & 0b11
    accumulated_down = (accumulated_dir >> DOWNSHIFT) & 0b11
    accumulated_left = (accumulated_dir >> LEFTSHIFT) & 0b11
    if row > 1 && accumulated_up ≠ 3 && accumulated_down == 0
        push!(ret, (row - 1, col, (accumulated_up + 1) << UPSHIFT))
    end
    if row < rows && accumulated_down ≠ 3 && accumulated_up == 0
        push!(ret, (row + 1, col, (accumulated_down + 1) << DOWNSHIFT))
    end
    if col > 1 && accumulated_left ≠ 3 && accumulated_right == 0
        push!(ret, (row, col - 1, (accumulated_left + 1) << LEFTSHIFT))
    end
    if col < cols && accumulated_right ≠ 3 && accumulated_left == 0
        push!(ret, (row, col + 1, (accumulated_right + 1) << RIGHTSHIFT))
    end
    ret
end

function isgoal(state, goal)
    state[1] == goal[1] && state[2] == goal[2]
end

function heuristic(start, goal)
    abs(goal[1] - start[1]) + abs(goal[2] - start[2])
end

function cost(lines, current, neighbour)
    lines[neighbour[1]][neighbour[2]]
end

function main()
    lines = readlines("input")
    lines = parse_line.(lines)

    rows = length(lines)
    cols = length(lines[1])
    n = s -> neighbours(lines, s)
    # state: (row, col, accumulated direction)
    start::Tuple{Int, Int, UInt8} = (1, 1, 0)
    goal::Tuple{Int, Int, UInt8} = (rows, cols, 0)
    c = (c, n) -> cost(lines, c, n)
    res = astar(n, start, goal, heuristic=heuristic, cost=c, isgoal=isgoal)
    println(res.cost)
end

main()
