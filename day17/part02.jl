#!/usr/bin/env julia

using AStarSearch

function parse_line(line)
    parse.(Int, collect(line))
end

const UPSHIFT = 0
const RIGHTSHIFT = 4
const DOWNSHIFT = 8
const LEFTSHIFT = 12

const MINMOVE = 4
const MAXMOVE = 10

function neighbours(lines, state::Tuple{Int, Int, UInt16})::Array{Tuple{Int, Int, UInt16}}
    rows = length(lines)
    cols = length(lines[1])
    row, col, accumulated_dir = state
    ret = []
    accumulated_up = (accumulated_dir >> UPSHIFT) & 0b1111
    accumulated_right = (accumulated_dir >> RIGHTSHIFT) & 0b1111
    accumulated_down = (accumulated_dir >> DOWNSHIFT) & 0b1111
    accumulated_left = (accumulated_dir >> LEFTSHIFT) & 0b1111
    if accumulated_up ≠ MAXMOVE && accumulated_down == 0
        moves_to_go = accumulated_up == 0 ? MINMOVE : 1
        if row - moves_to_go ≥ 1
            push!(ret, (row - moves_to_go, col, (accumulated_up + moves_to_go) << UPSHIFT))
        end
    end
    if accumulated_down ≠ MAXMOVE && accumulated_up == 0
        moves_to_go = accumulated_down == 0 ? MINMOVE : 1
        if row + moves_to_go ≤ rows
            push!(ret, (row + moves_to_go, col, (accumulated_down + moves_to_go) << DOWNSHIFT))
        end
    end
    if accumulated_left ≠ MAXMOVE && accumulated_right == 0
        moves_to_go = accumulated_left == 0 ? MINMOVE : 1
        if col - moves_to_go ≥ 1
            push!(ret, (row, col - moves_to_go, (accumulated_left + moves_to_go) << LEFTSHIFT))
        end
    end
    if accumulated_right ≠ MAXMOVE && accumulated_left == 0
        moves_to_go = accumulated_right == 0 ? MINMOVE : 1
        if col + moves_to_go ≤ cols
            push!(ret, (row, col + moves_to_go, (accumulated_right + moves_to_go) << RIGHTSHIFT))
        end
    end
    ret
end

function isgoal(state, goal)
    state[1] == goal[1] && state[2] == goal[2]
end

function heuristic(start, goal)
    abs(goal[1] - start[1]) + abs(goal[2] - start[2])
end

function cost(lines, current, neighbour)
    sum = 0
    rowstart = min(current[1], neighbour[1])
    rowend = max(current[1], neighbour[1])
    colstart = min(current[2], neighbour[2])
    colend = max(current[2], neighbour[2])
    for row in rowstart:rowend
        for col in colstart:colend
            if row ≠ current[1] || col ≠ current[2]
                sum += lines[row][col]
            end
        end
    end
    sum
end

function main()
    lines = readlines("input")
    lines = parse_line.(lines)

    rows = length(lines)
    cols = length(lines[1])
    n = s -> neighbours(lines, s)
    # state: (row, col, accumulated direction)
    start::Tuple{Int, Int, UInt16} = (1, 1, 0)
    goal::Tuple{Int, Int, UInt16} = (rows, cols, 0)
    c = (c, n) -> cost(lines, c, n)
    res = astar(n, start, goal, heuristic=heuristic, cost=c, isgoal=isgoal)
    println(res.path)
    println(res.cost)
end

main()
