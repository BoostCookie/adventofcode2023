#!/usr/bin/env julia

function hash(str, init)
    for char in str
        init += Int(char)
        init *= 17
        init %= 256
    end
    init
end

function main()
    input = readlines("input")[1]
    sum = 0
    for sequence in split(input, ",")
        sum += hash(sequence, 0)
    end
    println(sum)
end

main()
