#!/usr/bin/env julia

function hash(str, init)
    for char ∈ str
        init += Int(char)
        init *= 17
        init %= 256
    end
    init
end

function seq_to_box!(boxes, sequence)
    if occursin("-", sequence)
        label = sequence[1:end-1]
        lhash = hash(label, 0)
        if lhash ∈ keys(boxes)
            for (i, (cur_label, _)) ∈ enumerate(boxes[lhash])
                if cur_label == label
                    deleteat!(boxes[lhash], i)
                    break
                end
            end
        end
    else
        label, focal_length = split(sequence, "=")
        focal_length = parse(Int, focal_length)
        lhash = hash(label, 0)
        if lhash ∉ keys(boxes)
            boxes[lhash] = []
        end
        done = false
        for (slot, (cur_label, _)) ∈ enumerate(boxes[lhash])
            if cur_label == label
                boxes[lhash][slot] = (cur_label, focal_length)
                done = true
                break
            end
        end
        if !done
            push!(boxes[lhash], (label, focal_length))
        end
    end
end

function main()
    boxes::Dict{Int, Array{Tuple{AbstractString, Int}}} = Dict()
    input = readlines("input")[1]
    for sequence ∈ split(input, ",")
        seq_to_box!(boxes, sequence)
    end
    sum = 0
    for boxnum ∈ keys(boxes)
        for (slot, (_, focal_length)) ∈ enumerate(boxes[boxnum])
            sum += (boxnum + 1) * slot * focal_length
        end
    end
    println(sum)
end

main()
