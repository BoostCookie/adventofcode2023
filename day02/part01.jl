#!/usr/bin/env julia

function get_run(run_str::AbstractString)
    red, green, blue = 0, 0, 0
    for num_colour ∈ split(run_str, ", ")
        num, colour = split(num_colour, " ")
        num = parse(Int, num)
        if colour == "red"; red += num
        elseif colour == "green"; green += num
        else blue += num; end
    end
    red, green, blue
end

function get_game(line::AbstractString)
    colon_i = findfirst(':', line)
    id = parse(Int, line[(length("Game ") + 1):(colon_i - 1)])
    runs = map(get_run, split(line[colon_i + 2:end], "; "))
    id, runs
end

function run_is_possible(run, max_red, max_green, max_blue)
    red, green, blue = run
    red ≤ max_red && green ≤ max_green && blue ≤ max_blue
end

function main()
    sum = 0
    for line ∈ readlines("input")
        id, runs = get_game(line)
        if all(run -> run_is_possible(run, 12, 13, 14), runs)
            sum += id
        end
    end
    println(sum)
end

if abspath(PROGRAM_FILE) == @__FILE__
    main()
end
