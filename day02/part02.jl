#!/usr/bin/env julia

function get_run(run_str::AbstractString)
    red, green, blue = 0, 0, 0
    for num_colour ∈ split(run_str, ", ")
        num, colour = split(num_colour, " ")
        num = parse(Int, num)
        if colour == "red"; red += num
        elseif colour == "green"; green += num
        else blue += num; end
    end
    red, green, blue
end

function get_game(line::AbstractString)
    colon_i = findfirst(':', line)
    id = parse(Int, line[(length("Game ") + 1):(colon_i - 1)])
    runs = map(get_run, split(line[colon_i + 2:end], "; "))
    id, runs
end

function min_cubes_of_game(runs)
    min_red, min_green, min_blue = 0, 0, 0
    for run ∈ runs
        red, green, blue = run
        min_red = max(red, min_red)
        min_green = max(green, min_green)
        min_blue = max(blue, min_blue)
    end
    min_red, min_green, min_blue
end

function main()
    sum = 0
    for line ∈ readlines("input")
        _, runs = get_game(line)
        min_red, min_green, min_blue = min_cubes_of_game(runs)
        sum += min_red * min_green * min_blue
    end
    println(sum)
end

if abspath(PROGRAM_FILE) == @__FILE__
    main()
end
