#!/usr/bin/env julia

function visual_to_nums(visual::AbstractString)
    nums::Array{Int} = []
    damaged_indices = findall('#', visual)
    prev_damaged = -1
    for i in damaged_indices
        if i ≠ prev_damaged + 1
            push!(nums, 1)
        else
            nums[end] += 1
        end
        prev_damaged = i
    end
    nums
end

function get_row(line::AbstractString)
    visual, nums = split(line, " ")
    nums = parse.(Int, split(nums, ","))
    visual, nums
end

function main()
    lines = readlines("input")
    rows = get_row.(lines)
    sum = 0
    for (visual, nums) in rows
        @show visual
        @show nums
        unknown_indices = findall('?', visual)
        working_visual = collect(replace(visual, "?" => "."))
        end_encountered = true
        while end_encountered
            if working_visual |> join |> visual_to_nums == nums
                sum += 1
            end
            end_encountered = false
            for i in unknown_indices
                if working_visual[i] == '.'
                    working_visual[i] = '#'
                    end_encountered = true
                    break
                else
                    working_visual[i] = '.'
                end
            end
        end
    end
    println(sum)
end

main()
