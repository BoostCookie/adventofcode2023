#!/usr/bin/env julia

# insert the strength of the type to the front of the deck
function type_value(cards)
    card_counting = Dict(14 => 0, 13 => 0, 12 => 0, 1 => 0, 10 => 0, 9 => 0, 8 => 0,
                       7 => 0,  6 => 0,  5 => 0,  4 => 0,  3 => 0, 2 => 0)
    for card ∈ cards
        card_counting[card] += 1
    end
    num_of_jokers = card_counting[1]
    card_counting[1] = 0
    count_values = card_counting |> values |> collect |> sort
    highest, second_highest = count_values[end] + num_of_jokers, count_values[end - 1]

    if highest == 5
        7
    elseif highest == 4
        6
    elseif highest == 3 && second_highest == 2
        5
    elseif highest == 3
        4
    elseif highest == 2 && second_highest == 2
        3
    elseif highest == 2
        2
    else
        1
    end
end

function card_value(card)
    card_strength = Dict('A' => 14, 'K' => 13, 'Q' => 12, 'J' => 1, 'T' => 10, '9' => 9,
                         '8' => 8, '7' => 7, '6' => 6, '5' => 5, '4' => 4, '3' => 3, '2' => 2)
    card_strength[card]
end

function read_hand(line)
    cards, bid = split(line, " ")
    cards = cards |> collect .|> card_value
    bid = parse(Int, bid)
    type_value(cards), cards, bid
end

function main()
    lines = readlines("input")
    hands = read_hand.(lines)
    sort!(hands)
    sum = 0
    for (i, hand) ∈ enumerate(hands)
        sum += i * hand[end]
    end
    println(sum)
end

main()
