#!/usr/bin/env julia

# insert the strength of the type to the front of the deck
function type_value(cards)
    card_counting = Dict(14 => 0, 13 => 0, 12 => 0, 11 => 0, 10 => 0, 9 => 0, 8 => 0,
                       7 => 0,  6 => 0,  5 => 0,  4 => 0,  3 => 0, 2 => 0)
    count_to_value = Dict((5, 0) => 7, (4, 1) => 6, (3, 2) => 5, (3, 1) => 4, (2, 2) => 3, (2, 1) => 2, (1, 1) => 1)
    for card ∈ cards
        card_counting[card] += 1
    end
    count_values = card_counting |> values |> collect |> sort
    highest, second_highest = count_values[end], count_values[end - 1]
    count_to_value[(highest, second_highest)]
end

function card_value(card)
    card_strength = Dict('A' => 14, 'K' => 13, 'Q' => 12, 'J' => 11, 'T' => 10, '9' => 9,
                         '8' => 8, '7' => 7, '6' => 6, '5' => 5, '4' => 4, '3' => 3, '2' => 2)
    card_strength[card]
end

function read_hand(line)
    cards, bid = split(line, " ")
    cards = cards |> collect .|> card_value
    bid = parse(Int, bid)
    type_value(cards), cards, bid
end

function main()
    lines = readlines("input")
    hands = read_hand.(lines)
    sort!(hands)
    sum = 0
    for (i, hand) ∈ enumerate(hands)
        sum += i * hand[end]
    end
    println(sum)
end

main()
