#!/usr/bin/env julia

function get_mapping(maps, mapname, source)
    for sourcerange ∈ keys(maps[mapname])
        if source ∈ sourcerange
            return maps[mapname][sourcerange] + source - first(sourcerange)
        end
    end
    source
end

function numstr_to_list(numstr::AbstractString)
    parse.(Int, split(numstr, " "))
end

function get_almanac(lines)
    seeds = []
    maps = Dict("seed-to-soil"=>Dict(), "soil-to-fertilizer"=>Dict(), "fertilizer-to-water"=>Dict(), "water-to-light"=>Dict(), "light-to-temperature"=>Dict(), "temperature-to-humidity"=>Dict(), "humidity-to-location"=>Dict())
    currentmap = nothing
    for line ∈ lines
        if isempty(line); continue; end
        if startswith(line, "seeds: ")
            # first line => get the required seeds
            seeds = numstr_to_list(line[length("seeds: ")+1:end])
        elseif ':' ∈ line
            # start of a new map
            currentmap = split(line, " ")[1]
        else
            # reading ∈ the current map
            destination_start, source_start, range_length = numstr_to_list(line)
            maps[currentmap][source_start:source_start+range_length-1] = destination_start
        end
    end
    seeds, maps
end

function main()
    input = readlines("input")
    seeds, maps = get_almanac(input)

    seed_to_soil(s) = get_mapping(maps, "seed-to-soil", s)
    soil_to_fertiliser(s) = get_mapping(maps, "soil-to-fertilizer", s)
    fertiliser_to_water(s) = get_mapping(maps, "fertilizer-to-water", s)
    water_to_light(s) = get_mapping(maps, "water-to-light", s)
    light_to_temperature(s) = get_mapping(maps, "light-to-temperature", s)
    temperature_to_humidity(s) = get_mapping(maps, "temperature-to-humidity", s)
    humidity_to_location(s) = get_mapping(maps, "humidity-to-location", s)

    @show seeds
    locations = seeds .|> seed_to_soil .|> soil_to_fertiliser .|> fertiliser_to_water .|> water_to_light .|> light_to_temperature .|> temperature_to_humidity .|> humidity_to_location
    @show locations
    println(minimum(locations))
end

main()
