#!/usr/bin/env julia

function map_ranges(map::Dict{UnitRange{Int64}, Int64}, inputranges::Vector{UnitRange{Int64}})
    map_sources = map |> keys |> collect |> sort
    sort!(inputranges)
    cur_sourcerange, state = iterate(map_sources)
    dest_ranges::Vector{UnitRange{Int64}} = []
    for inputrange ∈ inputranges
        inputrangeleft = inputrange
        if cur_sourcerange === nothing
            @label no_cur_sourcerange
            push!(dest_ranges, inputrangeleft)
            continue
        end
        # iterate map range until it is ahead of sourcerange
        @label advance_cur_sourcerange
        while first(inputrangeleft) > last(cur_sourcerange)
            iter = iterate(map_sources, state)
            if iter === nothing
                cur_sourcerange = nothing
                @goto no_cur_sourcerange
            end
            cur_sourcerange, state = iter
        end
        # identity map for elements below cur_sourcerange
        if first(inputrangeleft) < first(cur_sourcerange)
            push!(dest_ranges, first(inputrangeleft):min(last(inputrangeleft), first(cur_sourcerange) - 1))
            if last(inputrangeleft) < first(cur_sourcerange)
                continue
            end
            inputrangeleft = first(cur_sourcerange):last(inputrangeleft)
        end
        # map for elements in cur_sourcerange
        map_dest_range_start = map[cur_sourcerange]
        start_distance = first(inputrangeleft) - first(cur_sourcerange)
        input_is_bigger = last(inputrangeleft) > last(cur_sourcerange)
        end_distance = (input_is_bigger ? last(cur_sourcerange) : last(inputrangeleft)) - first(cur_sourcerange)
        push!(dest_ranges, map_dest_range_start+start_distance:map_dest_range_start+end_distance)
        # advance cur_sourcerange for input values above it
        if input_is_bigger
            inputrangeleft = last(cur_sourcerange)+1:last(inputrangeleft)
            @goto advance_cur_sourcerange
        end
    end
    dest_ranges
end

function numstr_to_list(numstr::AbstractString)
    parse.(Int, split(numstr, " "))
end

function get_almanac(lines)
    seedranges = []
    maps::Dict{AbstractString, Dict{UnitRange{Int64}, Int64}} =
        Dict("seed-to-soil"=>Dict(), "soil-to-fertilizer"=>Dict(), "fertilizer-to-water"=>Dict(),
             "water-to-light"=>Dict(), "light-to-temperature"=>Dict(), "temperature-to-humidity"=>Dict(),
             "humidity-to-location"=>Dict())
    currentmap = nothing
    for line ∈ lines
        if isempty(line); continue; end
        if startswith(line, "seeds: ")
            # first line => get the required seeds
            seed_nums = numstr_to_list(line[length("seeds: ")+1:end])
            seed_rangestarts = seed_nums[1:2:end]
            seed_rangelengths = seed_nums[2:2:end]
            seedranges = map(i -> seed_rangestarts[i]:seed_rangestarts[i]+seed_rangelengths[i]-1, 1:length(seed_rangestarts))
        elseif ':' ∈ line
            # start of a new map
            currentmap = split(line, " ")[1]
        else
            # reading in the current map
            destination_start, source_start, range_length = numstr_to_list(line)
            maps[currentmap][source_start:source_start+range_length-1] = destination_start
        end
    end
    seedranges, maps
end

function main()
    input = readlines("input")
    seedranges, maps = get_almanac(input)

    seed_to_soil(s) = map_ranges(maps["seed-to-soil"], s)
    soil_to_fertiliser(s) = map_ranges(maps["soil-to-fertilizer"], s)
    fertiliser_to_water(s) = map_ranges(maps["fertilizer-to-water"], s)
    water_to_light(s) = map_ranges(maps["water-to-light"], s)
    light_to_temperature(s) = map_ranges(maps["light-to-temperature"], s)
    temperature_to_humidity(s) = map_ranges(maps["temperature-to-humidity"], s)
    humidity_to_location(s) = map_ranges(maps["humidity-to-location"], s)

    location_ranges = seedranges |> seed_to_soil |> soil_to_fertiliser |> fertiliser_to_water |>
                        water_to_light |> light_to_temperature |> temperature_to_humidity |>
                        humidity_to_location
    println(first(minimum(location_ranges)))
end

main()
