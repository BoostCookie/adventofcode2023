#!/usr/bin/env julia

function get_nums(numstr::AbstractString)
    parse.(Int, split(numstr, " ") |> filter(!isempty))
end

function get_card(line::AbstractString)
    card, rest = split(line, ": ")
    card = parse(Int, card[length("Card "):end])
    winningnums, havenums = split(rest, " | ") .|> get_nums
    card, winningnums, havenums
end

function count_wins(winningnums, havenums)::Int
    count(w -> w ∈ havenums, winningnums)
end

function main()
    input = readlines("input")
    copiesofcards = ones(Int, length(input))
    for line ∈ input
        card, winningnums, havenums = get_card(line)
        numofwins = count_wins(winningnums, havenums)
        copiesofcards[card+1:card+numofwins] .+= copiesofcards[card]
    end
    copiesofcards |> sum |> println
end

if abspath(PROGRAM_FILE) == @__FILE__
    main()
end
