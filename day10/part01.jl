#!/usr/bin/env julia

@enum Direction right up left down

function get_starting_pos(lines)
    startline_i, startline = iterate(filter(l -> 'S' ∈ l[2], lines |> enumerate |> collect))[1]
    startline_i, findfirst('S', startline)
end

function go_to_direction(lines, row, col, direction)
    if direction == up && row ≠ 1
        row -= 1
    elseif direction == down && row ≠ length(lines)
        row += 1
    elseif direction == right && col ≠ length(lines[row])
        col += 1
    elseif direction == left && col ≠ 1
        col -= 1
    else
        return false, row, col
    end
    true, row, col
end

# for finding the start after 'S'
function validpipe(symbol, prevstep)
    (prevstep == right && (symbol == '-' || symbol == '7' || symbol == 'J')) ||
    (prevstep == up && (symbol == '|' || symbol == '7' || symbol == 'F')) ||
    (prevstep == left && (symbol == '-' || symbol == 'F' || symbol == 'L')) ||
    (prevstep == down && (symbol == '|' || symbol == 'J' || symbol == 'L'))
end

# we are now at `symbol` and the last step was `prevstep`
# => which direction is the next step?
function get_next_step(symbol, prevstep)
    if prevstep == right
        if symbol == '-'
            right
        elseif symbol == '7'
            down
        elseif symbol == 'J'
            up
        end
    elseif prevstep == up
        if symbol == '|'
            up
        elseif symbol == '7'
            left
        elseif symbol == 'F'
            right
        end
    elseif prevstep == left
        if symbol == '-'
            left
        elseif symbol == 'F'
            down
        elseif symbol == 'L'
            up
        end
    elseif prevstep == down
        if symbol == '|'
            down
        elseif symbol == 'L'
            right
        elseif symbol == 'J'
            left
        end
    end
end

function looplength(lines, startrow, startcol)
    loop = [(startrow, startcol)]
    prevstep = right
    row, col = startrow, startcol
    # find a valid direction from the start
    for d ∈ instances(Direction)
        validdir, row, col = go_to_direction(lines, startrow, startcol, d)
        if validdir && validpipe(lines[row][col], d)
            push!(loop, (row, col))
            prevstep = d
            break
        end
    end

    while true
        curstep = get_next_step(lines[row][col], prevstep)
        _, row, col = go_to_direction(lines, row, col, curstep)
        if lines[row][col] == 'S'
            break
        end
        push!(loop, (row, col))
        prevstep = curstep
    end
    length(loop)
end

function main()
    lines = readlines("input")
    startrow, startcol = get_starting_pos(lines)
    looplength(lines, startrow, startcol) ÷ 2 |> println
end

main()
