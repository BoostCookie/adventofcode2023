#!/usr/bin/env julia

@enum Direction right up left down

function get_starting_pos(lines)
    startline_i, startline = iterate(filter(l -> 'S' ∈ l[2], lines |> enumerate |> collect))[1]
    startline_i, findfirst('S', startline)
end

function go_to_direction(lines, row, col, direction)
    if direction == up && row ≠ 1
        row -= 1
    elseif direction == down && row ≠ length(lines)
        row += 1
    elseif direction == right && col ≠ length(lines[row])
        col += 1
    elseif direction == left && col ≠ 1
        col -= 1
    else
        return false, row, col
    end
    true, row, col
end

# for finding the start after 'S'
function validpipe(symbol, prevstep)
    (prevstep == right && (symbol == '-' || symbol == '7' || symbol == 'J')) ||
    (prevstep == up && (symbol == '|' || symbol == '7' || symbol == 'F')) ||
    (prevstep == left && (symbol == '-' || symbol == 'F' || symbol == 'L')) ||
    (prevstep == down && (symbol == '|' || symbol == 'J' || symbol == 'L'))
end

# we are now at `symbol` and the last step was `prevstep`
# => which direction is the next step?
function get_next_step(symbol, prevstep)
    if prevstep == right
        if symbol == '-'
            right
        elseif symbol == '7'
            down
        elseif symbol == 'J'
            up
        end
    elseif prevstep == up
        if symbol == '|'
            up
        elseif symbol == '7'
            left
        elseif symbol == 'F'
            right
        end
    elseif prevstep == left
        if symbol == '-'
            left
        elseif symbol == 'F'
            down
        elseif symbol == 'L'
            up
        end
    elseif prevstep == down
        if symbol == '|'
            down
        elseif symbol == 'L'
            right
        elseif symbol == 'J'
            left
        end
    end
end

#=
function pairwise(iterable)
    zip(iterable[1:end-1], iterable[2:end])
end

function get_general_turn(directions)
    numofleftturns = 0
    numofrightturns = 0
    for (dold, dnew) ∈ pairwise(directions)
        if (dold == right && dnew == down) ||
            (dold == up && dnew == right) ||
            (dold == left && dnew == up) ||
            (dold == down && dnew == left)
            numofrightturns += 1
        elseif (dold == right && dnew == up) ||
            (dold == up && dnew == left) ||
            (dold == left && dnew == down) ||
            (dold == down && dnew == right)
            numofleftturns += 1
        end
    end
    numofrightturns > numofrightturns
end
=#

# returns the loop and whether it has a general right or a left turn
function get_loop(lines, startrow, startcol)
    loop = [(startrow, startcol)]
    directions::Array{Direction} = []
    prevstep = right
    row, col = startrow, startcol
    # find a valid direction from the start
    for d ∈ instances(Direction)
        validdir, row, col = go_to_direction(lines, startrow, startcol, d)
        if validdir && validpipe(lines[row][col], d)
            push!(loop, (row, col))
            push!(directions, d)
            prevstep = d
            break
        end
    end

    while true
        curstep = get_next_step(lines[row][col], prevstep)
        _, row, col = go_to_direction(lines, row, col, curstep)
        push!(directions, curstep)
        if lines[row][col] == 'S'
            break
        end
        push!(loop, (row, col))
        prevstep = curstep
    end
    loop
end

function count_loop_inside(lines, loop)
    # go from top to bottom, left to right
    # every time we cross over a boundary of the loop, we start counting
    # when we get over a boundary again, then we stop counting
    count = 0
    for row ∈ minimum(loop)[1]+1:maximum(loop)[1]-1
        encounter_came_from = up
        we_are_inside = false
        for col ∈ 1:length(lines[row])
            symbol = lines[row][col]
            if (row, col) ∈ loop
                # directly walking over a pipe of the loop
                # => flip `we_are_inside`
                if symbol == '|'
                    we_are_inside = !we_are_inside
                # by walking from left to right we just encountered
                # a part of the loop where it takes a turn
                # => remember from where it came
                elseif symbol == 'L'
                    encounter_came_from = up
                elseif symbol == 'F'
                    encounter_came_from = down
                # we are at the end of our walk along the pipe
                # => flip `we_are_inside` if we actually walked OVER the pipe
                elseif (symbol == 'J' && encounter_came_from == down) ||
                    (symbol == '7' && encounter_came_from == up)
                    we_are_inside = !we_are_inside
                end
            elseif we_are_inside
                count += 1
            end
        end
    end
    count
end

function main()
    lines = readlines("input")
    startrow, startcol = get_starting_pos(lines)
    loop = get_loop(lines, startrow, startcol)
    count_loop_inside(lines, loop) |> println
end

main()
