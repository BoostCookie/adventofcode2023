#!/usr/bin/env julia

function get_instruction(line)
    _, hexinfo = split(line, " (#")
    hexinfo = hexinfo[1:end-1]
    distance = parse(Int, "0x" * hexinfo[1:5])
    direction = hexinfo[6:6]
    direction = replace(direction, "0" => "R", "1" => "D", "2" => "L", "3" => "U")[1]
    @show direction, distance
    direction, distance
end

function walk(row, col, direction, distance)
    if direction == 'R'
        row, col+distance
    elseif direction == 'U'
        row-distance, col
    elseif direction == 'D'
        row+distance, col
    elseif direction == 'L'
        row, col-distance
    else
        println("What is this direction? $direction")
        exit()
    end
end

function get_positive_corners(instructions)::Array{Tuple{Int, Int}}
    row, col = 0, 0
    corners = []
    for (direction, distance) ∈ instructions
        row, col = walk(row, col, direction, distance)
        push!(corners, (row, col))
    end
    if row ≠ 0 || col ≠ 0
        println("This is not a closed loop!")
        exit()
    end
    corners
end

# it is even simpler than that, because we are dealing with rectangles, not trapezoids
function gauss_shoelace(corners)
    (r1, c1), (r2, c2) = corners[1:2]
    # either the first two corners are from a horizontal or from a vertical line
    start_index = 1
    if r1 ≠ r2
        # we are only interested in horizontal lines
        start_index = 2
    end

    sum = 0
    for i ∈ start_index:2:length(corners)
        r1, c1 = corners[i]
        i2 = i+1
        if i2 > length(corners)
            i2 = 1
        end
        r2, c2 = corners[i2]
        if r1 ≠ r2
            println("You done fucked up!")
            exit()
        end
        sum += (c2 - c1) * r1
    end
    abs(sum)
end

function missed_border_area(corners)
    # number of border fields
    n = 0
    for i ∈ 1:length(corners)
        r1, c1 = corners[i]
        i2 = i+1
        if i2 > length(corners)
            i2 = 1
        end
        r2, c2 = corners[i2]
        n += max(abs(r2 - r1), abs(c2 - c1))
    end
    n ÷ 2 + 1
end

function main()
    lines = readlines("input")
    instructions = get_instruction.(lines)
    corners = get_positive_corners(instructions)
    gauss_shoelace(corners) + missed_border_area(corners) |> println
end

main()
