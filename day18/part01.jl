#!/usr/bin/env julia

function dig_instruction(line)
    direction, distance, colour = split(line, " ")
    direction = direction[1]
    distance = parse(Int, distance)
    colour = colour[3:end-1]
    direction, distance, colour
end

function walk(row, col, direction, distance)
    if direction == 'R'
        row, col+distance
    elseif direction == 'U'
        row-distance, col
    elseif direction == 'D'
        row+distance, col
    else
        row, col-distance
    end
end

function get_height_width_start(instructions)
    row, col = 1, 1
    minrow, mincol = 1, 1
    maxrow, maxcol = 1, 1
    for (direction, distance, _) in instructions
        row, col = walk(row, col, direction, distance)
        if row < minrow
            minrow = row
        end
        if col < mincol
            mincol = col
        end
        if row > maxrow
            maxrow = row
        end
        if col > maxcol
            maxcol = col
        end
    end
    maxrow - minrow + 1, maxcol - mincol + 1, -minrow+2, -mincol+2
end

function fill_matrix!(matrix, startrow, startcol, instructions)
    for (direction, distance, _) in instructions
        row, col = walk(startrow, startcol, direction, distance)
        matrix[min(startrow, row):max(startrow, row), min(startcol, col):max(startcol, col)] .= 1
        startrow, startcol = row, col
    end
end

function dig_interior!(matrix)
    rows, cols = size(matrix)
    for row in 2:rows-1
        inside = false
        camefrom = ' '
        col = 1
        while col ≤ cols
            if matrix[row, col] == 1
                # border detected
                bordercolmax = col
                while bordercolmax < cols && matrix[row, bordercolmax+1] == 1
                    bordercolmax += 1
                end
                if col == bordercolmax || matrix[row-1, col] ≠ matrix[row-1, bordercolmax]
                    inside = !inside
                end
                col = bordercolmax
            else
                if inside
                    matrix[row, col] = -1
                end
            end
            col += 1
        end
    end
end

function main()
    lines = readlines("input")
    instructions = dig_instruction.(lines)
    height, width, startrow, startcol = get_height_width_start(instructions)
    matrix = zeros(Int, height, width)
    fill_matrix!(matrix, startrow, startcol, instructions)
    #display(matrix)
    #matrix .|> abs |> sum |> println
    dig_interior!(matrix)
    #display(matrix)
    matrix .|> abs |> sum |> println
end

main()
